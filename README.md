# RealSense Calibration Flasher From Plex

This utility takes the values from a calibrated Plex and flashes them onto an Intel RealSense.

## Setup

Note: This tool has only been testing on Linux machines. Those operating on Windows should use
discretion when attempting to run this tool.

### Plex Structure

A Plex is a JSON file produced by Tangram Vision's calibration software, TVCal. It contains the full
calibration state of your system at the time of processing. One can produce a plex from a TVCal
output by running the following [jq](https://jqlang.github.io/jq/) command:

```shell
less output.json | jq .plex > plex.json
```

This RealSense Flasher tool, as written, will look for two components with these specific names in
your plex:

- "rs_ir_left": The leftmost IR camera on the RealSense, when facing the line of sight of the
  cameras
- "rs_ir_right": The rightmost IR camera on the RealSense

The easiest way to ensure this during calibration is to label the data streams from each of these
cameras accordingly. If the data is coming from a ROSbag, name the topics "rs_ir_left" and
"rs_ir_right". If it is coming from folder sets, name the folders "rs_ir_left" and "rs_ir_right".

### Dependency - RealSense

This program expects that librealsense2 is installed on your local machine. We recommend using the
pre-built package for
[Linux](https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md).

### Dependency - RealSensee Dynamic Calibration Tool

This program will look for the [Intel® RealSense™ D400 Series Dynamic Calibration
Tool](https://www.intel.com/content/www/us/en/download/645988/intel-realsense-d400-series-dynamic-calibration-tool.html)
on your local machine. Make sure to download and install this utility before attempting to build the
program.

This program will look for the utility at this location by default:

```shell
/usr/share/doc/librscalibrationtool/api/DynamicCalibrationAPI-Linux-2.13.1.0.tar.gz
```

One can change this location by modifying the CMakeLists.txt.

### Build

```shell
cd <this directory>
mkdir build && cd build
cmake ..
make
```

## Usage

```shell
./flash_calibration <path_to_plex.json>
```

Make sure that your RealSense is plugged in!