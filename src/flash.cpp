// clang-format off
#include "fmt/format.h"
#include <cstddef>
#include <DSDynamicCalibration.h>
// clang-format on

#include <Eigen/Dense>
#include <Eigen/Geometry>
#include <chrono>
#include <cmath>
#include <filesystem>
#include <fmt/core.h>
#include <fmt/ostream.h>
#include <fmt/ranges.h>
#include <fstream>
#include <iostream>
#include <librealsense2/rs.hpp>
#include <nlohmann/json.hpp>
#include <sstream>
#include <thread>

using nlohmann::json;

int main(int argc, char** argv)
{
    rs2::pipeline pipeline;
    rs2::config config;
    rs2::context ctx;
    rs2::device_list devices = ctx.query_devices();
    auto device = devices[0];
    std::this_thread::sleep_for(std::chrono::milliseconds(500));

    auto dc = DynamicCalibrationAPI::DSDynamicCalibration();
    const auto init_ret = dc.Initialize(&device,
        DynamicCalibrationAPI::DSDynamicCalibration::CalibrationMode::CAL_MODE_USER_CUSTOM,
        1280, 800);

    // Read the current calibration parameters
    {
        int resolutionLeftRight[2];
        double focalLengthLeft[2];
        double principalPointLeft[2];
        double distortionLeft[5];
        double focalLengthRight[2];
        double principalPointRight[2];
        double distortionRight[5];
        double rotationRight[9];
        double translationRight[3];
        bool hasRGB;
        int resolutionRGB[2];
        double focalLengthRGB[2];
        double principalPointRGB[2];
        double distortionRGB[5];
        double rotationRGB[9];
        double translationRGB[3];

        const auto read_ret = dc.ReadCalibrationParameters(
            resolutionLeftRight, focalLengthLeft, principalPointLeft,
            distortionLeft, focalLengthRight, principalPointRight, distortionRight,
            rotationRight, translationRight, hasRGB, resolutionRGB, focalLengthRGB,
            principalPointRGB, distortionRGB, rotationRGB, translationRGB);

        fmt::println("Current Calibration:");
        fmt::println("Resolution: {}", resolutionLeftRight);
        fmt::println("Left Focal: {}", focalLengthLeft);
        fmt::println("Left Principle Point: {}", principalPointLeft);
        fmt::println("Left Distortion: {}", distortionLeft);
        fmt::println("Right Focal: {}", focalLengthRight);
        fmt::println("Right Principle Point: {}", principalPointRight);
        fmt::println("Right Distortion: {}", distortionRight);
        fmt::println("Translation : {}", translationRight);
        fmt::println("Rotation: {}", rotationRight);
    }

    if (argc != 2) {
        fmt::println(stderr, "There should be an argument pointing to the input plex");
        return 1;
    }

    const auto plex_path = std::filesystem::path(argv[1]);
    std::ifstream f(plex_path);
    json data = json::parse(f);

    json plex;
    if (data.contains("plex")) {
        fmt::println("Json has a \"plex\" key so we'll interpret it as Umbra output");
        plex = data["plex"];
    } else {
        plex = data;
    }

    {
        int resolutionLeftRight[2] = { 1280, 800 };
        double focalLengthLeft[2];
        double principalPointLeft[2];
        double distortionLeft[5];
        double focalLengthRight[2];
        double principalPointRight[2];
        double distortionRight[5];
        double rotationRight[9];
        double translationRight[3];
        bool hasRGB = false;
        int resolutionRGB[2] = { 0, 0 };
        double focalLengthRGB[2] = { 0., 0. };
        double principalPointRGB[2] = { 0., 0. };
        double distortionRGB[5] = { 0., 0., 0., 0., 0. };
        double rotationRGB[9] = { 0., 0., 0., 0., 0., 0., 0., 0., 0. };
        double translationRGB[3] = { 0., 0., 0. };

        const auto& components = plex["components"];

        bool foundLeft = false;
        bool foundRight = false;
        std::string leftUuid;
        std::string rightUuid;
        for (const auto& c : components) {
            if (!c.contains("camera")) {
                continue;
            }
            const auto& cam = c["camera"];
            const auto& name = cam["name"].get<std::string>();
            const auto& intrinsics = cam["intrinsics"];
            const auto& pinhole = intrinsics["projection"]["pinhole"];
            if (!intrinsics.contains("distortion") || !intrinsics["distortion"].contains("opencv_brown_conrady")) {
                fmt::println("{}: doesn't have opencv bc", name);
                continue;
            }
            const auto& distortion = intrinsics["distortion"]["opencv_brown_conrady"];
            if (name == "rs_ir_left") {
                foundLeft = true;
                leftUuid = cam["uuid"];

                pinhole["f"].get_to(focalLengthLeft[0]);
                pinhole["f"].get_to(focalLengthLeft[1]);
                pinhole["cx"].get_to(principalPointLeft[0]);
                pinhole["cy"].get_to(principalPointLeft[1]);

                // k1, k2, p1, p2, k3
                distortion["k1"].get_to(distortionLeft[0]);
                distortion["k2"].get_to(distortionLeft[1]);
                distortion["k3"].get_to(distortionLeft[4]);
                distortion["p1"].get_to(distortionLeft[2]);
                distortion["p2"].get_to(distortionLeft[3]);

            } else if (name == "rs_ir_right") {
                foundRight = true;
                rightUuid = cam["uuid"];

                pinhole["f"].get_to(focalLengthRight[0]);
                pinhole["f"].get_to(focalLengthRight[1]);
                pinhole["cx"].get_to(principalPointRight[0]);
                pinhole["cy"].get_to(principalPointRight[1]);

                // k1, k2, p1, p2, k3
                distortion["k1"].get_to(distortionRight[0]);
                distortion["k2"].get_to(distortionRight[1]);
                distortion["k3"].get_to(distortionRight[4]);
                distortion["p1"].get_to(distortionRight[2]);
                distortion["p2"].get_to(distortionRight[3]);
            }
        }

        if (!foundLeft || !foundRight) {
            fmt::println(stderr, "Input doesn't contain both a left and right camera");
            return 1;
        }

        // Find this spatial constraint
        const auto& scs = plex["spatial_constraints"];
        const auto sc_itr = std::find_if(scs.begin(), scs.end(), [&](const json& sc) {
            const auto& from = sc["from"].get<std::string>();
            const auto& to = sc["to"].get<std::string>();
            return (from == leftUuid && to == rightUuid) || (from == rightUuid && to == leftUuid);
        });
        if (sc_itr == scs.end()) {
            fmt::println(stderr, "No spatial constraint found");
            return 1;
        }
        const auto& sc = *sc_itr;
        const auto& extrinsics = sc["extrinsics"];
        const auto& quat_json = extrinsics["rotation"];
        const auto& trans_json = extrinsics["translation"];
        fmt::println("{}", trans_json.dump(2));

        const bool shouldInvert = sc["to"].get<std::string>() == leftUuid;

        // w, x, y, z
        auto quat = Eigen::Quaterniond(quat_json[3], quat_json[0], quat_json[1], quat_json[2]);
        constexpr double metersToMillimeters = 1000.0;
        auto trans = Eigen::Vector3d(trans_json[0], trans_json[1], trans_json[2]) * metersToMillimeters;

        Eigen::Isometry3d iso = Eigen::Translation3d(trans) * quat;
        if (shouldInvert) {
            fmt::println("This listed spatial constraint is the inverse of what RealSense needs");
            iso = iso.inverse();
        }

        std::stringstream s;
        s << iso.matrix();
        fmt::println("matrix: {}", s.str());

        for (size_t i = 0, r = 0; r < 3; ++r) {
            for (size_t c = 0; c < 3; ++c, ++i) {
                rotationRight[i] = iso.matrix()(r, c);
            }
            translationRight[r] = iso.matrix()(r, 3);
        }

        fmt::println("New Calibration:");
        fmt::println("Resolution: {}", resolutionLeftRight);
        fmt::println("Left Focal: {}", focalLengthLeft);
        fmt::println("Left Principle Point: {}", principalPointLeft);
        fmt::println("Left Distortion: {}", distortionLeft);
        fmt::println("Right Focal: {}", focalLengthRight);
        fmt::println("Right Principle Point: {}", principalPointRight);
        fmt::println("Right Distortion: {}", distortionRight);
        fmt::println("Translation : {}", translationRight);
        fmt::println("Rotation: {}", rotationRight);

        const auto write_ret = dc.WriteCustomCalibrationParameters(
            resolutionLeftRight, focalLengthLeft, principalPointLeft,
            distortionLeft, focalLengthRight, principalPointRight, distortionRight,
            rotationRight, translationRight, hasRGB, resolutionRGB, focalLengthRGB,
            principalPointRGB, distortionRGB, rotationRGB, translationRGB);

        fmt::println("Write result: {}", write_ret);
    }

    return 0;
}
